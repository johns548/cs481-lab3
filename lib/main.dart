import 'package:flutter/material.dart';
import 'chip.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  static const String _title = 'Lab 3 - Group Class Roster';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        appBar: AppBar(title: const Text(_title)),
        body: myChip(),
      ),
    );
  }
}
