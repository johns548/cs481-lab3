import 'package:flutter/material.dart';

class myChip extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _myChipState();
}

class _myChipState extends State<myChip> {

  bool _dataTables = false;

  Widget table(){
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: FittedBox(
        child: DataTable(
          columns: <DataColumn>[
            DataColumn(label: Text("Name")),
            DataColumn(label: Text("Course 1")),
            DataColumn(label: Text("Course 2")),
            DataColumn(label: Text("Course 3")),
            DataColumn(label: Text("Course 4")),
          ],
          rows: <DataRow>[
            DataRow(
              cells: [
                DataCell(Text("Pedro")),
                DataCell(Text("CS436")),
                DataCell(Text("CS443")),
                DataCell(Text("CS478")),
                DataCell(Text("CS481"))
              ],
            ),
            DataRow(
              cells: [
                DataCell(Text("Alex")),
                DataCell(Text("CS441")),
                DataCell(Text("FMST300")),
                DataCell(Text("MATH374")),
                DataCell(Text("CS481"))
              ],
            ),
            DataRow(
              cells: [
                DataCell(Text("Aaron")),
                DataCell(Text("CS421")),
                DataCell(Text("CS441")),
                DataCell(Text("BIO104")),
                DataCell(Text("CS481"))
              ],
            ),
            DataRow(
              cells: [
                DataCell(Text("Ryann")),
                DataCell(Text("CS441")),
                DataCell(Text("CS421")),
                DataCell(Text("CS478")),
                DataCell(Text("CS481"))
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildCard1() {
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const ListTile(
              leading: Icon(Icons.school_outlined),
              title: Text('Pedro - Total Classes:'),
              subtitle: Text('4 Courses total - 4 CS'),
            ),
          ],
        ),
      ),
    );
  }
  Widget buildCard2() {
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const ListTile(
              leading: Icon(Icons.school_outlined),
              title: Text('Alex - Total Classes:'),
              subtitle: Text('4 Courses total - 2 CS, 1 FMST, 1 MATH'),
            ),
          ],
        ),
      ),
    );
  }
  Widget buildCard3() {
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const ListTile(
              leading: Icon(Icons.school_outlined),
              title: Text('Aaron - Total Classes:'),
              subtitle: Text('4 Courses total - 3 CS, 1 BIO'),
            ),
          ],
        ),
      ),
    );
  }
  Widget buildCard4() {
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const ListTile(
              leading: Icon(Icons.school_outlined),
              title: Text('Ryann - Total Classes:'),
              subtitle: Text('4 Courses total - 4 CS'),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildTablePage() {
    return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Here are the courses we are taking!"),
            buildCard1(),
            buildCard2(),
            buildCard3(),
            buildCard4(),
            table(),
            RaisedButton(
              onPressed: () {
                setState(() => _dataTables = !_dataTables);
              },
              child: Text("Back to the home page"),
            ),
          ],
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _dataTables ? buildTablePage() : Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Chip(
              avatar: CircleAvatar(
                backgroundColor: Colors.black,
                child: Text('P'),
              ),
              label: Text('Pedro'),
            ),
            Chip(
              avatar: CircleAvatar(
                backgroundColor: Colors.black,
                child: Text('R'),
              ),
              label: Text('Ryann'),
            ),
            Chip(
              avatar: CircleAvatar(
                backgroundColor: Colors.black,
                child: Text('A'),
              ),
              label: Text('Alex'),
            ),
            Chip(
              avatar: CircleAvatar(
                backgroundColor: Colors.black,
                child: Text('A'),
              ),
              label: Text('Aaron'),
            ),
            RaisedButton(
              onPressed: () {
                setState(() => _dataTables = !_dataTables);
                },
              child: Text("What Courses Are We Taking?"),
            ),
          ],
        ),
      ),
    );
  }
}
